$(document).ready(function() {
  const base_url="https://api.weather.gov/stations/";
  const endpoint="/observations/latest";

  // weather update button click
  $('#getwx').on('click', function(e) {
    var mystation = $('input').val();
    var myurl = base_url + mystation + endpoint;
    $('input#my-url').val(myurl);

    // clear out any previous data
    $('ul li').each(function() {
      $('ul').empty();
    });

    console.log("Cleared Elements of UL");

    // execute AJAX call to get and render data
    $.ajax({
      url: myurl,
      dataType: "json",
      success: function(data) {
        var tempC= data['properties']['temperature'].value.toFixed(1);
        var tempF = (tempC * 9/5 + 32).toFixed(1);

        // get wind info and convert m/s to kts
        var windDirection = data['properties']['windDirection'].value;
        var windSpeed = data['properties']['windSpeed'].value;
        var dewPoint = data['properties']['dewpoint'].value.toFixed(1);
        var iconImage = data['properties']['icon'].value;


        // uncomment this if you want to dump full JSON to textarea
        // var myJSON = JSON.stringify(data);
        // $('textarea').val(myJSON);

        var currIcon = "<li>Icon:<img src=\"" + iconImage + "\"" + "</li>";
        $('ul').append(currIcon);
        $('ul li:last').attr('class', 'list-group-item');

        var currTemp = "<li>Current temperature: " + tempC +"C & Farenheight:: " + tempF+"F"+"</li>";
        $('ul').append(currTemp);
        $('ul li:last').attr('class', 'list-group-item');

        // Direction
        var wDirection = "<li>The current wind direction:: "
          + windDirection + "</li>";
        $('ul').append(wDirection);
        $('ul li:last').attr('class', 'list-group-item');

        // Speed
        var wSpeed = "<li>The current wind speed:: "
          + windSpeed + "</li>";
        $('ul').append(wSpeed);
        $('ul li:last').attr('class', 'list-group-item');

        // Dewpoint (relative humidity)
        var dpoint = "<li>The current dewpoint:: "
          + dewPoint + "</li>";
        $('ul').append(dpoint);
        $('ul li:last').attr('class', 'list-group-item');
      }
    });
  });
});
