// Setting constants.
/*
* The API here returns a new json object that contains data about
* a current event.
*/
const newsFeedURL = 'https://newsapi.org/v2/everything?' +
                  'q=hacked&' +
                  'from=2019-03-19&' +
                  'sortBy=popularity&' +
                  'apiKey=17c241789deb4b82a2183e271cbaa13b';

var requestPromise = new Request(newsFeedURL);
/*
* Fetch method is the updated use of XMLHttpRequest.
* Will make a request to the url argument and returns object
* containing values that can be parsed.
*/
fetch(requestPromise)
  .then(function(response){
    return response.json();
  })
  .then(function(data){
    for(let article in data.articles){
      let newList = document.createElement('ul');
      mainFeedBody.appendChild(newList);
      let author = document.createElement('li');

      /*BEGIN DATA*/
      author.innerHTML = "<b>Author: </b>" + data.articles[article].author;
      console.log(data.articles[article]);
      newList.appendChild(author);
      //
      let content = document.createElement('li');
      content.innerHTML = "<b>Content: </b>" + data.articles[article].content;
      newList.appendChild(content);
      //
      let description = document.createElement('li');
      description.innerHTML = "<b>Description: </b>" + data.articles[article].description;
      newList.appendChild(description);
      //
      let publishedAt = document.createElement('li');
      publishedAt.innerHTML = "<b>Published Date: </b>" + data.articles[article].publishedAt;
      newList.appendChild(publishedAt);
      //
      let sourceName = document.createElement('li');
      sourceName.innerHTML = "<b>Source: </b>" + data.articles[article].source.name;
      //
      let title = document.createElement('li');
      title.innerHTML = "<b>Title: </b>" + data.articles[article].title;
      newList.appendChild(title);
      //
      let url = document.createElement('li');
      url.innerHTML = "<b>URL:</b>" + "<a href=\"" + data.articles[article].url + "\"" + ">" + "</a>";
      newList.appendChild(url);
      //
      let urlToImage = document.createElement('li');
      urlToImage.innerHTML = "<b>Image: </b>" + "<img src=\"" + data.articles[article].urlToImage + "\"" + ">" + "</img>";
      newList.appendChild(urlToImage);
      /*END DATA*/
    }
  })
