const baseURL = "https://api.coindesk.com/v1/bpi/currentprice/";

// Grabs the index value (0-8) of the selected option in the list.
var currencyList = document.getElementById("currencyList");
var currencyCode = currencyList.options[currencyList.selectedIndex];

var chartData = [];

var morris_chart_obj = {
  element: "price-chart",
  data: "",
  xkey: "time",
  ykeys: ["price"]
};

function getData() {
  $.ajax({
    url: myURL,
    success: function(data) {
      console.log(myURL);
      renderGraph(data);
      printPrice(data);
    }
  });
}

function renderGraph(data) {
  plotMyData(parseData(data));
}

function printPrice() {}

function parseData(textPriceData) {
  let bcJSON = JSON.parse(textPriceData);
  let rateText = bcJSON.bpi[currencyCode.text].rate;
  rateText = rateText.replace(/,/g, "");
  return parseFloat(rateText);
}

var priceChart = new Morris.Line(morris_chart_obj);

function updateData(price) {
  const maxChartValues = 10;
  var newEvent = {
    time: Date.now(),
    price: price
  };

  chartData.push(newEvent);
  if (chartData.length > maxChartValues) {
    chartData.shift();
  }
}

function plotMyData(data) {
  updateData(data);
  priceChart.setData(chartData);
  priceChart.redraw();
}

function changeUrl(){
  currencyCode = currencyList.options[currencyList.selectedIndex];
  let newCode = currencyCode.text;
  myURL = baseURL + newCode + ".json";
  return myURL;
}

// Dynamic setup of 'myURL' by setting value to returned
// value of function that's called when event occurs in selection list.
var myURL = changeUrl();


setInterval(getData, 1000);
