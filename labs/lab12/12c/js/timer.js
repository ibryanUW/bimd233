// Time is measured in milliseconds
var executionDelay = setInterval(fireEvery3Seconds, 3000);
var countTheExecutions = 0;

function fireEvery3Seconds() {
  countTheExecutions += 1;
  document.getElementById("functionOutput").innerHTML = countTheExecutions;
}
