function loadCarTable() {
  // Find the div for the table and create a table inside it with a body
  var tableDiv = document.getElementById("carTableDiv");
  var table = document.createElement("TABLE");
  var tableBody = document.createElement("tbody");

  // Append the body to the table so no overwrite happens.
  table.appendChild(tableBody);


  var car1 = ["hyundai", "Sonta", 2009, 3500];
  var car2 = ["ford", "focus", 2011, 45000.99];
  var car3 = ["toyota", "prius", 2018, 100000.99];
  var car4 = ["nissan", "leaf", 2015, 90000.99];
  var car5 = ["audi", "s8", 2015, 120000.99];

  var cars = [car1, car2, car3, car4, car5];

  // Table Rows
  for (var i = 0; i < cars.length; i++) {
    var tr = document.createElement("TR");
    for (var j = 0; j < cars[i].length; j++) {
      var td = document.createElement("TD");
      td.appendChild(document.createTextNode(cars[i][j]));
      tr.appendChild(td)
    }
    tableBody.appendChild(tr);
  }
  tableDiv.appendChild(table);
}
