/*
* This functions takes in a value through the 'radius' parameter and
* evaluates multiple values related to a circle.
* @parameter radius - Holds input value from html page.
* @return circleValues - Array of values related to a circle.
*/
function calcCircleGeometries(radius) {
  const pi = Math.PI;
  // Basic math operations for a circle's properties.
  var area = pi * radius * radius;
  var circumference = 2 * pi * radius;
  var diameter = 2 * radius;
  // Store the values in an array
  var circleValues = [area, circumference, diameter];
  return circleValues;
}

/*
* Executes each time the button on screen is pressed.
* Begins with generating random value for radius between 0 and 10 (not inclusive)
* and assigns the returned values to HTML elements.
* HTML elements are filled using `.innerHTML`.
*/
function showData(){
  var randomRadius = (Math.random() * 10) + 1;
  // Array is returned and stored in variable to be parsed.
  var tempData = calcCircleGeometries(randomRadius);

  // Generate a <li> in the top level list.
  var returnedValues = document.createElement("LI");
  var newData = document.createTextNode(tempData);

  //
  for(var i = 0; i < tempData.length; i++){
    var dataElement = document.createTextNode("Value: " + tempData[i]);
    document.getElementById("placeholder").innerHTML = dataElement;
  }

  // 
  returnedValues.appendChild(newData);
  document.getElementById("data").appendChild(returnedValues);
}
