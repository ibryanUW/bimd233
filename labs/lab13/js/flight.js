/*
* Constructor function takes in parameters and declares new
* variables with those values.
*/
function Flight(a, n, o, d, dt, at, ag) {
  this.airline = a;
  this.number = n;
  this.origin = o;
  this.destination = d;
  this.departure_time = dt;
  this.arrival_time = at;
  this.arrival_gate = ag;
  // Call to anonymous function that holds and passes two values
  // through to the GetTimeDiff function.
  this.flight_duration = function() {
    return GetTimeDiff(this.departure_time, this.arrival_time);
  };
}

// Compute the differnce between two UTC times in (fractional) hours
// should return the string value of above in HH:MM:SS format
function GetTimeDiff(t1, t2) {
  var d1 = new Date(t1);
  var d2 = new Date(t2);
  // console.log(t1, t2);
  return parseFloat((d2 - d1) / 1000 / 3600).toFixed(2); // Divide by 1000 = Convert to seconds, 3600 = minutes
}

 function GetOneRowofHTML(i) {
     return "<tr><td>" +
  flights[i].airline +
  "</td><td>" +
  flights[i].number +
  "</td><td>" +
  flights[i].origin +
  "</td><td>" +
  flights[i].destination +
  "</td><td>" +
  flights[i].departure_time +
  "</td><td>" +
  flights[i].arrival_time +
  "</td><td>" +
  flights[i].arrival_gate +
  "</td><td>" +
  flights[i].flight_duration() +
  "</td></tr>";
 }

/* Object #1 (flight #1) with data from https://flightaware.com/live/ */
var f1 = new Flight(
  "JZA (Jazz Airline)",
  8043,
  "Vancouver, CA",
  "Sacramento, CA",
  "MAR 06, 2019 13:36:00",
  "MAR 06, 2019 15:26:00",
  "E84"
);
var f2 = new Flight(
  "ACA (Air Canada)",
  554,
  "Vancouver, CA",
  "Los Angeles, CA",
  "MAR 06, 2019 14:44:00",
  "MAR 06, 2019 17:46:00",
  "69B"
);
var f3 = new Flight(
  "AA (American Airlines)",
  500,
  "Seattle, WA",
  "Phoenix, AZ",
  "MAR 06, 2019 14:37:00",
  "MAR 06, 2019 18:42:00",
  "A22"
);
var f4 = new Flight(
  "QZE (Horizon)",
  267,
  "Las Vegas, NV",
  "Everett, WA",
  "MAR 06, 2019 12:37:00",
  "MAR 06, 2019 15:30:00",
  "1"
);
var f5 = new Flight(
  "CPA (Cathay Pacific)",
  846,
  "Hong Kong",
  "New York, NY",
  "MAR 06, 2019 20:12:00",
  "MAR 06, 2019 22:19:00",
  "Terminal 8"
);

// Array of flights
var flights = [f1, f2, f3, f4, f5];

var el = document.getElementById("mytable");

el.innerHTML = "";

var i = 0;

el.innerHTML += GetOneRowofHTML(0);
el.innerHTML += GetOneRowofHTML(1);
el.innerHTML += GetOneRowofHTML(2);
el.innerHTML += GetOneRowofHTML(3);
el.innerHTML += GetOneRowofHTML(4);
