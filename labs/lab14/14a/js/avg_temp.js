/* Array of Objects that contain same properties. */
let wx_data = [
  {
    day: "TH",
    high: 43,
    low: 33
  },
  {
    day: "FRI",
    high: 44,
    low: 32
  },
  {
    day: "SAT",
    high: 47,
    low: 34
  },
  {
    day: "SUN",
    high: 50,
    low: 29
  },
  {
    day: "MON",
    high: 49,
    low: 29
  }
];

let avgHigh;
let avgLow;
let avgAll;
let allHighs = [];
let allLows = [];

function getAverages(total, num){
  return total + num;
}

// Add highs to separate array
for(var i=0; i<wx_data.length; i++){
  allHighs[i] = wx_data[i].high;
}

// Add lows to separate array
for(var j=0; j<wx_data.length; j++){
  allLows[j] = wx_data[j].low;
}

avgHigh = allHighs.reduce(getAverages) / allHighs.length;
avgLow = allLows.reduce(getAverages) / allLows.length;
avgAll = (avgHigh + avgLow) / 2;

let tableData = document.getElementById("data_table");
tableData.innerHTML = "";
tableData.innerHTML += "<tr><td>" + avgHigh + "</td><td>" + avgLow + "</td><td>" + avgAll + "</td></tr>";
