// Begin program
  // Prompt user for input
    // User enters string value
  // program evaluates response
    // Do - prompt the user for string value
    // While - string values does not match 'exit' or 'quit'
/**********************************************************/
function stateMachine(){
  let state = "idle";
  let stateTracker = 0;
  var cmd = prompt("Enter a command: " + state,
  "Type 'next' to advance or type 'quit' or 'exit' to stop.");

  if(cmd == "next" || cmd == "Next"){
    do {
      switch(stateTracker){
        case 0:
          state = "s1";
          break;
        case 1:
          state = "s2";
          break;
        case 2:
          state = "s3";
      }

      stateTracker++;
      if(stateTracker >= 3){
        stateTracker = 0;
      }

      cmd = prompt("Enter a command: " + state,
      "Type 'next' to advance or type 'quit' or 'exit' to stop.");

    }while(cmd != "quit" || cmd != "exit");
  }else{
    alert("Incorrect input");
  }
}
