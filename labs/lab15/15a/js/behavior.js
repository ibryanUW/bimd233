/* Top 10 Pacific Northwest Publicly Traded Companies
* ranked by Seattle Times.
*/
let microsoft = {
  name: "Microsft",
  marketCap: 381.7,
  sales: 86.8,
  profit: 22.07,
  employeeCount: 128000
};

let symetraFinancial = {
  name: "Symetra Financial",
  marketCap: 2.7,
  sales: 2.2,
  profit: 2.18,
  employeeCount: 1400
};

let micronTechnology = {
  name: "Micron Technology",
  marketCap: 37.6,
  sales: 16.4,
  profit: 30.39,
  employeeCount: 30400
};

let f5Networks = {
  name: "F5 Networks",
  marketCap: 9.5,
  sales: 1.7,
  profit: 0.4029,
  employeeCount: 3834
};

let expedia = {
  name: "Expedia",
  marketCap: 10.8,
  sales: 5.8,
  profit: 0.3981,
  employeeCount: 18210
};

let nautilus = {
  name: "Nautilus",
  marketCap: 0.476,
  sales: 0.2744,
  profit: 0.0238,
  employeeCount: 480
};

let heritageFinancial = {
  name: "Heritage Financial",
  marketCap: 0.531,
  sales: 0.1376,
  profit: 0.09801,
  employeeCount: 16
};

let cascadeMicrotech = {
  name: "Cascade Microtech",
  marketCap: 0.239,
  sales: 0.136,
  profit: 0.004303,
  employeeCount: 401
};

let nike = {
  name: "Nike",
  marketCap: 83.1,
  sales: 27.8,
  profit: 7.4,
  employeeCount: 56500
};

let alaskaAirGroup = {
  name: "Alaska Air Group",
  marketCap: 7.9,
  sales: 5.4,
  profit: 0.605,
  employeeCount: 13000
};

/* Making an array to store the 10 companies */
let topTenCompanies = [microsoft, symetraFinancial, micronTechnology, f5Networks, expedia, nautilus, heritageFinancial, cascadeMicrotech, nike, alaskaAirGroup];

var mainTableBody = document.getElementById("tableBody");

mainTableBody.innerHTML = "";

function fillRows(){
  for(var company in topTenCompanies){
    tableBody.innerHTML += "<tr><td>" +
    topTenCompanies[company].name + "</td><td>" +
    topTenCompanies[company].marketCap + "</td><td>" +
    topTenCompanies[company].sales + "</td><td>" +
    topTenCompanies[company].profit + "</td><td>" +
    topTenCompanies[company].employeeCount + "</td></tr>";
  }
}
