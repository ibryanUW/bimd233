/*API Key for Events: R3WDmd9LSM7KGCM2*/
/*
Example url for getting music events:
http://eventful.com/events?q=music&l=Seattle
http://eventful.com/events?q=progressive&l=Seattle&c=music
*/
// Setting constants and global variables.
var eventfulEvents = [];

/*
* The API here returns a new json object that contains data about
* a current event in Seattle.
* The '/json/' portion of the call allows the return object to already be parsed
* into a JSON format. (I found this very convienent).
*/
const eventfulFeed = 'http://api.eventful.com/json/events/search?' +
                    'app_key=R3WDmd9LSM7KGCM2&' +
                    'q=music&' +
                    'l=Seattle';
/*
*
*/
$.ajax({
  url: eventfulFeed,
  dataType: 'jsonp',
  success: function(apiResponse){
    // TODO
    // Call function to parse array's objects
    eventfulEvents = apiResponse.events.event;
    fillElements(eventfulEvents);
  }
});

/*
*
*/
function fillElements(newData){
  // Create body (appended to classes, id's are for css only)
  $("<div class='card-body' id='eventCardBody'></div>").appendTo('.card');
  // Loop over objects and append (fill) in corresponding DOM elements.
  for(let event in newData){
    /*
    * newData is the 0th element of the 0th element of the JSON object array.
    * Looping over the elements to create their corresponding DOM pieces.
    * id's ending in "NI" means 'no-indent' used for css selection.
    * id's endind in "I" means 'indent' used for css seletion.
    */
    // Make card title for the event thumbnail and append it to the card wrapper.
    $("<div class='card-title' id='eventTitle'>" +
    "Event: " + newData[event].title + "</div>").appendTo(".card-body");

    // All the general information about the event.
    $("<div class='card-text' id='cityName'>" +
    "Event Location City: " + newData[event].city_name + "</div>").appendTo('.card-body');

    $("<div class='card-text' id='startTime'>" +
    "Event Start Time: " + newData[event].start_time + "</div>").appendTo('.card-body');

    $("<div class='card-text' id='venueName'>" +
    "Venu Name: " + newData[event].venue_name + "</div>").appendTo('.card-body');

    $("<a class='btn btn-info' id='eventUrl' role='button'>See More Here</a>").appendTo('.card-body');
    $('#eventUrl').attr('href', newData[event].url);
  }
}
