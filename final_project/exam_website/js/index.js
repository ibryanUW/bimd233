// File: tmdb_api.js
const search_base_url = "https://api.themoviedb.org/3/search/movie?";
const poster_base_url = "https://image.tmdb.org/t/p/w500"

var movie_cell_template = "<div class='col-sm-3 movie-cell'><div class='movie-poster'><img></div><div class='movie-title'></div></div>";
var search_term = "";
var search_api_url;   // use this string to build your search from search_url and search_term

const api_key = "api_key=a4540206e3eb16e1c34ffe25f93f3e9c&"
const prefix="language=en-US&"
const suffix="&page=1&include_adult=false"

// use this function to concantenate (build) your search url
function getSearchUrl(queryString) {
  // Base url + custom string value + personal api key
  var url = search_base_url + api_key + prefix + "query=" + queryString + suffix;
  return url;
}

// use this function to concantenate (build) your poster url
function getPosterUrl(imageString) {
  var url = poster_base_url + imageString;
  return url;
}

// Shorthand for $( document ).ready()
$(function() {
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // ========================================================
  // TASK 1: key input processing
  // ========================================================
  $('input').on({
    // keydown - backspace or delete processing only
    keydown: function(e) {
      var code = e.which;
      if(code == 13){
        api_search();
      }
      // console.log('KEYDOWN-key:' + code);
    },
    // keypress - everything else processing
    keypress: function(e) {
      var code = e.which;
      if(code == 13){
        api_search();
      }
      // console.log('KEYPRESS-key:' + code);
    }
  });
  // ========================================================
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // ========================================================
  // TASK 2:  process search on tmdb
  // ========================================================
  $('#get-movie-btn').on('click', api_search);
    // console.log("Search button hit!");

  // Do the actual search in this function
  function api_search(e) {
    search_term = $('#userString').val();
    search_api_url = getSearchUrl(search_term);

    console.log(search_term);

    $('#search-url').val(search_api_url);
    // prepare search string url => search_api_url
    // add code to display the Search URL string on the input field with id="search-url"
    $.ajax({
      url: search_api_url,
      success: function(data) {
        render(data);
      },
      cache: false
    });
  }
  // ========================================================
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // ========================================================
  // TASK 3: perform all movie grid rendering operations
  // ========================================================
  function render(data) {
    // get movie data from search results data
    let movies = data.results;
    // select the movie grid and dump the sample html (if any)
    console.log(movies);
    for(let i=0; i<movies.length; i++){
      $('#movies-grid').append('<')
    }
    // utilize the movie_cell_template to append, then add data from
    // movie data results (parsed JSON)
  }
  // ========================================================
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
});
